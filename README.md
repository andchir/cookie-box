# CookieBox 1.0.0

![CookieBox - screenshot](https://gitlab.com/andchir/cookie-box/-/raw/master/preview.png "CookieBox - screenshot")

Usage example:
```html
<script src="cookie-box.js" ></script>
<script>
    var cookieBox = new CookieBox({
        htmlContent: 'For the site to work, you agree to our <a href="" target="_blank">Cookies Policy</a>.',
        storeStateDays: 0,
        width: '350px',
        backgroundColor: '#fff',
        borderRadius: '10px',
        left: '50px',
        bottom: '50px',
        fontFamily: 'Arial, Helvetica, sans-serif',
        buttonMargin: '5px 0 5px 10px',
        buttonColor: '#3e8ec7',
        buttonTextColor: '#fff',
        buttonBorderRadius: '6px',
        geoIpServiceUrl: '/cookie-box/geoip.php',
        onlyFor: [
            'BE', 'BG', 'CZ', 'DK', 'DE', 'EE', 'IE', 'EL', 'ES', 'FR', 'HR', 'IT', 'CY', 'LV',
            'LT', 'LU', 'HU', 'MT', 'NL', 'AT', 'PL', 'PT', 'RO', 'SI', 'SK', 'FI', 'SE'
        ]
    });
</script>
```
