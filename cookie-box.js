/**
 * CookieBox
 * @version 1.0.2
 * @author: Andchir<andchir@gmail.com>
 * @param options
 * @constructor
 */
(function (factory) {

    if ( typeof define === 'function' && define.amd ) {

        // AMD. Register as an anonymous module.
        define([], factory);

    } else if ( typeof exports === 'object' ) {

        // Node/CommonJS
        module.exports = factory();

    } else {

        // Browser globals
        window.CookieBox = factory();
    }

}(function( ){

    'use strict';

    function CookieBox (options) {

        var self = this, container;
        this.version = '1.0.2';

        var mainOptions = {
            debug: false,
            storeDataKey: 'cookie-box-hidden',
            htmlContent: 'For the site to work, you agree to our <a href="" target="_blank">Cookies Policy</a>.',
            storeStateDays: 0,
            width: '350px',
            backgroundColor: '#fff',
            borderRadius: '10px',
            left: '50px',
            bottom: '50px',
            fontFamily: 'Arial, Helvetica, sans-serif',
            buttonMargin: '5px 0 5px 10px',
            buttonColor: '#3e8ec7',
            buttonTextColor: '#fff',
            buttonBorderRadius: '6px',
            onlyFor: [],
            notFor: [],
            geoIpServiceUrl: '/cookie-box/geoip.php'
        };

        /**
         * Initialize
         */
        this.init = function () {
            this.extend(mainOptions, options);

            var boxHiddenValue = this.readCookie(mainOptions.storeDataKey);
            if (mainOptions.storeStateDays === 0) {
                this.deleteCookie(mainOptions.storeDataKey);
                boxHiddenValue = false;
            }
            if (!boxHiddenValue) {
                if (mainOptions.onlyFor.length > 0 || mainOptions.notFor.length > 0) {
                    this.ajax(mainOptions.geoIpServiceUrl, function(res) {
                        if (!res.countryCode) {
                            self.createContent();
                            return;
                        }
                        if (mainOptions.onlyFor.length > 0 && mainOptions.onlyFor.indexOf(res.countryCode) === -1) {
                            return;
                        }
                        if (mainOptions.notFor.length > 0 && mainOptions.notFor.indexOf(res.countryCode) > -1) {
                            return;
                        }
                        self.createContent();
                    });
                } else {
                    this.onReady(this.createContent.bind(this));
                }
            }
        };

        /**
         * Call function on content ready
         * @param {function} fn
         */
        this.onReady = function(fn) {
            this.debugLog('[ON_READY_STATE]', document.readyState);
            if (document.attachEvent
                ? document.readyState === 'complete'
                : document.readyState !== 'loading') {
                fn();
            } else {
                document.addEventListener('DOMContentLoaded', fn);
            }
        };

        this.createContent = function() {
            var buttonEl = this.createElement('button', {
                type: 'button',
                textContent: 'Accept'
            }, {
                float: 'right',
                padding: '5px 10px',
                margin: mainOptions.buttonMargin,
                backgroundColor: mainOptions.buttonColor,
                color: mainOptions.buttonTextColor,
                fontSize: '110%',
                border: '0',
                borderRadius: mainOptions.buttonBorderRadius,
                cursor: 'pointer'
            });
            var closeButtonEl = this.createElement('button', {
                type: 'button',
                innerHTML: '&times;'
            }, {
                position: 'absolute',
                top: '0',
                right: '0',
                border: '0',
                fontSize: '110%',
                backgroundColor: 'transparent',
                color: '#000',
                cursor: 'pointer'
            });
            var innerDiv = this.createElement('div', {
                innerHTML: mainOptions.htmlContent
            });
            var outerDiv = this.createElement('div', {}, {
                width: mainOptions.width,
                backgroundColor: mainOptions.backgroundColor,
                padding: '20px',
                borderRadius: mainOptions.borderRadius,
                fontSize: '16px',
                fontFamily: mainOptions.fontFamily,
                boxSizing: 'border-box',
                position: 'relative',
                left: mainOptions.left,
                bottom: mainOptions.bottom
            });
            container = this.createElement('div', {}, {
                backfaceVisibility: 'hidden',
                WebkitBackfaceVisibility: 'hidden',
                position: 'fixed',
                left: '0',
                bottom: '0',
                boxSizing: 'border-box',
                width: '100%'
            });
            outerDiv.appendChild(closeButtonEl);
            outerDiv.appendChild(buttonEl);
            outerDiv.appendChild(innerDiv);
            outerDiv.appendChild(this.createElement('div', {}, {display: 'block', clear: 'both'}));
            container.appendChild(outerDiv);
            document.body.appendChild(container);

            buttonEl.addEventListener('click', function(e) {
                e.preventDefault();
                outerDiv.style.display = 'none';

                if (mainOptions.storeStateDays > 0) {
                    self.createCookie(mainOptions.storeDataKey, '1', mainOptions.storeStateDays);
                }
            }, false);

            closeButtonEl.addEventListener('click', function(e) {
                e.preventDefault();
                outerDiv.style.display = 'none';
            }, false);

            window.addEventListener('resize', this.onResize.bind(this));
            this.onResize();
        };

        this.onResize = function() {
            if (window.innerWidth <= 650) {
                container.style.padding = '0 20px';
                container.firstChild.style.left = '0';
                container.firstChild.style.width = '100%';
            } else {
                container.style.padding = '0';
                container.firstChild.style.left = mainOptions.left;
                container.firstChild.style.width = mainOptions.width;
            }
        };

        /**
         *
         * @param tagName
         * @param attributes
         * @param css
         * @returns {HTMLElement}
         */
        this.createElement = function(tagName, attributes, css) {
            var el = document.createElement(tagName);
            if (attributes) {
                Object.keys(attributes).forEach(function (key) {
                    if (attributes[key] !== null) {
                        if (['className', 'innerHTML', 'outerHTML', 'innerText', 'textContent'].indexOf(key) > -1) {
                            el[key] = attributes[key];
                        } else {
                            el.setAttribute(key, attributes[key]);
                        }
                    }
                });
            }
            if (css) {
                Object.keys(css).forEach(function (key) {
                    el.style[key] = css[key];
                });
            }
            return el;
        };

        /**
         * Remove HTML element
         * @param {HTMLElement} el
         */
        this.removeEl = function(el) {
            el.parentNode.removeChild(el);
        };

        /**
         * Extend object
         * @param obj
         * @param props
         */
        this.extend = function(obj, props) {
            for (var prop in props) {
                if (props.hasOwnProperty(prop)) {
                    obj[prop] = props[prop];
                }
            }
        };

        /**
         * Ajax request
         * @param url
         * @param callback
         * @param data
         * @param type
         */
        this.ajax = function(url, callback, data, type) {
            type = type || 'GET';
            data = data || {};
            var request = new XMLHttpRequest();
            request.open(type, url, true);
            request.onload = function() {
                var resp = JSON.parse(request.responseText);
                if (request.status >= 200 && request.status < 400) {
                    if (!resp) {
                        resp = {};
                    }
                    resp.success = true;
                    self.debugLog('AJAX SUCCESS');
                    callback(resp);
                } else {
                    self.debugLog('AJAX FAIL');
                    callback({success: false, error: resp ? (resp.error || '') : ''});
                }
            };
            request.onerror = function(e) {
                self.debugLog(e);
            };
            if (data instanceof FormData) {
                request.send(data);
            } else {
                request.send(JSON.stringify(data));
            }
        };

        /**
         * Display debug info in console
         */
        this.debugLog = function() {
            if (mainOptions.debug) {
                console.log.apply(this, arguments);
            }
        };

        /** Create cookie */
        this.createCookie = function (name, value, days, path) {
            var expires;
            path = path || '/';
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = '; expires=' + date.toGMTString();
            }
            else {
                expires = '';
            }
            document.cookie = name + '=' + value + expires + '; path=' + path;
        };

        /** Read cookie value */
        this.readCookie = function (name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1, c.length);
                }
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        };

        /** Delete cookie */
        this.deleteCookie = function (name) {
            this.createCookie(name, '', -1);
        };

        this.init();
    }

    return CookieBox;
}));
