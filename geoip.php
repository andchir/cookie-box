<?php

use GeoIp2\Database\Reader;
use GeoIp2\Exception\GeoIp2Exception;
use MaxMind\Db\Reader\InvalidDatabaseException;

require __DIR__ . '/vendor/autoload.php';

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

try {
    $reader = new Reader(__DIR__ . '/geoip2/maxmind-db/GeoLite2-Country.mmdb');
} catch (InvalidDatabaseException $e) {
    echo 'ERROR: '. $e->getMessage(); exit;
}

$ipAddress = getRealIpAddr();

try {
    $record = $reader->country($ipAddress);
    $countryName = $record->country->name;
    $countryCode = $record->country->isoCode;
} catch (GeoIp2Exception $e) {
    $countryName = 'Unknown';
    $countryCode = '';
} catch (InvalidDatabaseException $e) {
    $countryName = 'Unknown';
    $countryCode = '';
}

echo json_encode(['countryName' => $countryName, 'countryCode' => $countryCode]);
